
const pass = document.querySelector('#password');
const confirm = document.querySelector('#confirm');
const showPass = document.querySelectorAll('.input-wrapper i');
const form = document.forms.form;

showPass.forEach(item => item.addEventListener('click', (ev) => {

    let inputEl = ev.target.parentElement.firstElementChild;

    if (inputEl.type === 'password') {
        inputEl.type = 'text';
        item.classList.remove('fa-eye');
        item.classList.add('fa-eye-slash');
    } else {
        inputEl.type = 'password';
        item.classList.remove('fa-eye-slash');
        item.classList.add('fa-eye');
    }
     
}));

form.addEventListener('submit', (ev) => {
    ev.preventDefault();
    const errorText = document.querySelector('.error-text');
    if (pass.value === confirm.value) {
        errorText.textContent = '';
        alert('You are welcome');      
    } else {
        errorText.textContent = 'Потрібно ввести однакові значення';
    }
})